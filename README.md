# Insert HTML plugins

This plugin aim to provide a way to insert HTML in places that is not covered
by [NodeBB widgets system](https://docs.nodebb.org/development/widgets/),
for example, places that are rendered dynamically.

Covered:

* Between posts (after every `n` posts)
* Inside \<head\> tag

## Installation

    npm install nodebb-plugin-insert-html

After installing and enabling this plugin, navigate to _Dashboard > Plugins >
Insert HTML_ to setup.
