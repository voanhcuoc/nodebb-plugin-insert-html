import controllers from './controllers';

export default (/* NodeBB modules injected here */) => (params, callback) => {
  const router = params.router;
  const hostMiddleware = params.middleware;
  // const hostControllers = params.controllers;

  // We create two routes for every view. One API call, and the actual route itself.
  // Just add the buildHeader middleware to your route and NodeBB will take care of
  // everything for you.

  router.get('/admin/plugins/insert-html', hostMiddleware.admin.buildHeader, controllers.renderAdminPage);
  router.get('/api/admin/plugins/insert-html', controllers.renderAdminPage);

  callback();
};
