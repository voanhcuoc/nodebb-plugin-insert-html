define('admin/plugins/insert-html', ['settings'], (Settings) => {
  const ACP = {};

  ACP.init = () => {
    const $form = $('.insert-html-settings');
    Settings.load('insert-html', $form);

    $('#save').on('click', () => {
      if ($form[0].checkValidity()) {
        Settings.save('insert-html', $('.insert-html-settings'), () => {
          app.alert({
            type: 'success',
            alert_id: 'insert-html-saved',
            title: 'Settings Saved',
            message: 'Please reload your NodeBB to apply these settings',
            clickfn() {
              socket.emit('admin.reload');
            },
          });
        });
      } else {
        app.alert({
          type: 'danger',
          alert_id: 'settings-save-failed',
          title: 'Failed to save settings',
          message: 'Please check form fields and click save again',
        });
      }
    });
  };

  return ACP;
});
