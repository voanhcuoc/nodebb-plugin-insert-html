define('insert-html', [], () => {
  $(window).on('action:ajaxify.end', () => {
    const settings = ajaxify.data.plugins['insert-html'];

    (function betweenPosts() {
      const every = parseInt(this.every, 10);
      const handler = () => {
        const tplName = ajaxify.data.template.name;
        if (tplName === 'topic' && this.html) {
          const $posts = $('[component="post"]')
            .filter(index => !(index % every))
            .filter((index, el) => !($(el).next().attr('component') === 'insert'));
          const $insert = $(`<div component="insert">${this.html}</div>`);
          $insert.insertAfter($posts);
        }
      };

      // now, if on topic page
      handler();
      // topic page is loaded
      $(window).on('action:topic.loaded', handler);
      // more posts is loaded when user scroll to the bottom of topic page
      $(window).on('action:posts.loaded', handler);
    }).call(settings.betweenPosts);
  });
});

require(['insert-html']);
