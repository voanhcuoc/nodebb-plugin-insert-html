<form role="form" class="insert-html-settings">
	<div id="betweenPosts" class="row">
		<div class="col-sm-2 col-xs-12 settings-header">Insert between forum posts</div>
		<div class="col-sm-10 col-xs-12">
			<div class="form-group">
				<label for="betweenPosts.every">Insert HTML every n posts: </label>
				<input type="text" id="betweenPosts.every" name="betweenPosts.every" title="Every" class="form-control"
          pattern="[0-9]*"
          required
          value="10">
			</div>
      <label for="betweenPosts.html">HTML: </label>
      <textarea id="betweenPosts.html" name="betweenPosts.html" title="HTML" class="form-control"></textarea>
		</div>
	</div>
  <div id="head" class="row">
    <div class="col-sm-2 col-xs-12 settings-header">Insert into <b>&lt;HEAD&gt;</b></div>
    <div class="col-sm-10 col-xs-12">
      <label for="head.html">HTML: </label>
      <textarea id="head.html" name="head.html" title="HTML" class="form-control"></textarea>
    </div>
  </div>
</form>

<button id="save" class="floating-button mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored">
	<i class="material-icons">save</i>
</button>
