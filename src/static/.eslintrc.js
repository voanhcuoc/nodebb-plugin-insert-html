module.exports = {
  env: {
    browser: true,
    jquery: true,
  },
  globals: {
    'ajaxify': true,
    'app': true,
    'define': true,
    'socket': true,
    'config': true,
  },
  rules: {
    'import/no-amd': 'off',
    'import/no-dynamic-require': 'off',
  }
};
