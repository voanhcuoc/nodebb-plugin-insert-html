/* eslint no-underscore-dangle: "off" */

import flat from 'flat';

import initWrapper from './lib/init';

const _meta = module.parent.require('./meta');

export const init = initWrapper();

export const addAdminNavigation = (header, callback) => {
  header.plugins.push({
    route: '/plugins/insert-html',
    icon: 'fa-bars',
    name: 'Insert HTML',
  });

  callback(null, header);
};

const settings = new Promise((resolve, reject) => {
  _meta.settings.get('insert-html', (err, result) => {
    if (err) reject(err);
    else resolve(flat.unflatten(result));
  });
});

export const appendSettings = (data, callback) => {
  const ajaxifyData = data.templateData;
  Promise.resolve(settings)
    .then((result) => {
      ajaxifyData.plugins = ajaxifyData.plugins || {};
      ajaxifyData.plugins['insert-html'] = result;
      callback(null, { templateData: ajaxifyData });
    })
    .catch(() => {
      callback(null, data);
    });
};

export const insertIntoHeader = ({ templateValues }, callback) => {
  /* eslint no-param-reassign: "off" */
  Promise.resolve(settings)
    .then((result) => {
      templateValues.useCustomJS = true;
      templateValues.customJS = templateValues.customJS || '';
      templateValues.customJS += result.head.html;
      callback(null, { templateValues });
    })
    .catch(() => {
      callback(null, { templateValues });
    });
};
